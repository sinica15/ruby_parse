module Write_csv

  def write (data_array, csv_name)
    puts Time.now.strftime("[%H:%M:%S]") + " writing results to file: #{csv_name}"

    # CSV_HEADER = ["Apples", "Oranges", "Pears"]
    csv_header = ["Names", "Prices", "Images"]


    directory_name = "temp_data"
    Dir.mkdir(directory_name) unless File.exists?(directory_name)
    @csv_name = "temp_data/" + csv_name + ".csv"



    def write_to_csv(row)
      if csv_exists?
        CSV.open(@csv_name, 'a+') { |csv| csv << row }
      else
        # create and add headers if doesn't exist already
        CSV.open(@csv_name, 'wb') do |csv|
          #csv << csv_header
          csv << row
        end
      end
    end

    def csv_exists?
      @exists ||= File.file?(@csv_name)
    end

    begin
      if csv_exists?
        CSV.open(@csv_name, 'wb') { |csv| csv << "" }
      end
    rescue StandardError => msg
      # time = Time.now.strftime("[%H:%M:%S]")
      # puts time + " oshibka " + msg
    end

    data_array.each { |row| write_to_csv(row) }

    puts Time.now.strftime("[%H:%M:%S]") + " wariting finished"
  end

end
