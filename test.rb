
a = 24.0
b = 0.1

# p a.class
# p b.class
# p (a*b).class

# p *a
# p *b
p (0.1 * 1.0) * (24.0 * 1) 

# p true.class, false.class
# p "Ruby".class
# p 1.class
# p 4.5.class
# p 3_463_456_457.class
# p :age.class
# p [1, 2, 3].class
# p h.class
