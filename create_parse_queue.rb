module Create_parse_queue

  def create (input_url)
    pages_queue = Array.new
    puts Time.now.strftime("[%H:%M:%S]") + " creating queue"

    for j in 1..10000

      url_req = input_url + '?p=' + j.to_s

      url = Curl.get(url_req) do |http|
          http.headers['User-Agent'] = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10; rv:33.0) Gecko/20100101 Firefox/33.0"
      end

      #break if j == 2
      break if url.body_str == ""

      doc = Nokogiri::HTML(url.body_str)

      doc.css('div.pro_first_box').each do |link|
        begin
          pages_queue << link.at_css('a').attr('href')
          puts Time.now.strftime("[%H:%M:%S]") + " added to queue: " + link.at_css('a').attr('href')
        rescue StandardError => msg
          puts Time.now.strftime("[%H:%M:%S]") + " some problems with product name, or src, or href: " + msg
        end
      end

    end

    puts Time.now.strftime("[%H:%M:%S]") + " queue created"
    pages_queue
  end

end
