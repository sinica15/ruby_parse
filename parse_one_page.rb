module Parse_one_page

  def parse(page_url, input_array)

    puts Time.now.strftime("[%H:%M:%S]") + " starting parse page: #{page_url}"

    url = Curl.get(page_url) do |http|
        http.headers['User-Agent'] = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10; rv:33.0) Gecko/20100101 Firefox/33.0"
    end

    doc = Nokogiri::HTML(url.body_str)

    name = ''
    pic = ''
    price_and_weight = Array.new

    name = doc.xpath('//*[@id="center_column"]/div/div[1]/div[2]/div[2]/h1').text.strip
    pic = doc.xpath('//*[@id="bigpic"]').attr('src')
    for i in 1..100
      break if doc.xpath('//*[@id="attributes"]/fieldset/div/ul/li[' + i.to_s + ']/label/span[1]').text.strip == ""
      couple_price_weight = Array.new
      for j in 1..2
        path = '//*[@id="attributes"]/fieldset/div/ul/li[' + i.to_s + ']/label/span[' + j.to_s + ']'
        couple_price_weight << doc.xpath(path).text.strip
      end
      price_and_weight << couple_price_weight
    end

    finished_array = Array.new
    for i in 0..price_and_weight.length - 1
      finished_array << [name + ' - ' + price_and_weight[i][0], price_and_weight[i][1], pic]
    end

  
      # finished_array.each do |ar_el|
      #   input_array << ar_el
      # end
    for i in 0..finished_array.length - 1
      input_array << finished_array[i]
    end


    puts Time.now.strftime("[%H:%M:%S]") + " writing result to array"
  end

end
