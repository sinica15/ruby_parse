require 'nokogiri'
require 'curb'
require 'fileutils'
require 'csv'
require_relative "parse_one_page.rb"
require_relative "create_parse_queue.rb"
require_relative "write_csv.rb"
include Write_csv
include Create_parse_queue
include Parse_one_page

url_input = ARGV[0]
file_name = ARGV[1]

url_input = 'https://www.petsonic.com/adiestramiento-para-perros/' if url_input == nil
file_name = 'output_data' if file_name == nil

puts Time.now.strftime("[%H:%M:%S]") + " start parsing " + url_input
puts Time.now.strftime("[%H:%M:%S]") + " output data will be saved in temp_data/" + url_input + ".csv"



parse_queue = Create_parse_queue.create(url_input)
output_data_array = Array.new


parse_queue_for_threads = Array.new(4)
for i in 0..parse_queue_for_threads.length - 1
  parse_queue_for_threads[i] = Array.new(2)
  for j in 0..parse_queue_for_threads[i].length - 1
    parse_queue_for_threads[i][j] = Array.new
  end
end

for i in 0..parse_queue.length - 1
  case i
  when 0..(parse_queue.length / 4) -1
    parse_queue_for_threads[0][0] << parse_queue[i]
  when (parse_queue.length / 4)..(parse_queue.length / 2) - 1
    parse_queue_for_threads[1][0] << parse_queue[i]
  when (parse_queue.length / 2)..(parse_queue.length / 4) * 3 - 1
    parse_queue_for_threads[2][0] << parse_queue[i]
  when ((parse_queue.length / 4) * 3 - 1)..parse_queue.length - 1
    parse_queue_for_threads[3][0] << parse_queue[i]
  end
end

threads = []
parse_queue_for_threads.each do |queue_for_threads_el|
  threads << Thread.new do
    for i in 0..queue_for_threads_el[0].length - 1
      Parse_one_page.parse(queue_for_threads_el[0][i], queue_for_threads_el[1])
    end
  end
end

threads.each(&:join)

parse_queue_for_threads.each do |queue_for_threads_res_el|
  queue_for_threads_res_el[1].each do |res_el|
    output_data_array << res_el
  end
end

Write_csv.write(output_data_array, file_name)
